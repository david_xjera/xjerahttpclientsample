package main

import (
	"bytes"
	"crypto/md5"
	"crypto/tls"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	pb "bitbucket.org/xjeralabs/gree/proto"
	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
)

func main() {
	if err := run(); err != nil {
		log.Println("error:", err)
		os.Exit(1)
	}
	os.Exit(0)
}

// Config a
type Config struct {
	ServerURL   string `json:"serverUrl"`
	ImgDir      string `json:"imgDir"`
	DumpRequest bool   `json:"dumpRequest"`
	NumRequest  int    `json:"numRequest"`
}

var defaultConfig = &Config{
	ServerURL:   "https://223.25.64.62:885/uploadTestData",
	ImgDir:      "images",
	DumpRequest: false,
	NumRequest:  5,
}

func run() error {
	//flag.StringVar(&serverUrl, "url", "https://223.25.64.62:885/uploadTestData", "server url")
	//flag.IntVar(&numRequest, "n", 10, "number of requests to send")
	//flag.StringVar(&imgDir, "i", "images", "images directory (all images will be sent in every request)")
	//flag.BoolVar(&dumpRequest, "dump", false, "save the request bytes and its md5 hash to a file")

	var cfgFile string

	flag.StringVar(&cfgFile, "cfg", "config.json", "config file")
	flag.Parse()

	cfg := defaultConfig

	cfgData, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		log.Println("read config file:", err)
	} else {
		if err := json.Unmarshal(cfgData, cfg); err != nil {
			return errors.Wrap(err, "json unmarshal")
		}
	}

	images, err := loadImages(cfg.ImgDir)
	if err != nil {
		return errors.Wrap(err, "load images")
	}
	log.Println(len(images), "images loaded")

	protoData := newProtoData(images)
	data, err := proto.Marshal(protoData)
	if err != nil {
		return errors.Wrap(err, "proto.Marshal")
	}

	//ioutil.WriteFile("protodata.txt", data, 0666)

	// base64 encode
	b64enc := base64.StdEncoding.EncodeToString([]byte(data))

	if cfg.DumpRequest {
		now := time.Now().UnixNano()
		filename := fmt.Sprintf("request_%v", now)

		if err := ioutil.WriteFile(filename+".txt", []byte(b64enc), 0666); err != nil {
			log.Println("write request error:", err)
		}

		md5sum := md5.Sum([]byte(b64enc))
		hash := hex.EncodeToString(md5sum[:])
		if err := ioutil.WriteFile(filename+".md5", []byte(hash), 0666); err != nil {
			log.Println("write md5 hash error:", err)
		}
	}

	sizeMB := float64(len(data)) / 1e6
	log.Printf("request size: %0.2fMB\n", sizeMB)

	readers := make([]io.Reader, cfg.NumRequest)
	for i := 0; i < cfg.NumRequest; i++ {
		readers[i] = bytes.NewBuffer([]byte(b64enc))
	}

	elapsed := sendRequests(readers, cfg.ServerURL)

	log.Println("sent", cfg.NumRequest, "requests")
	log.Println("total time taken:", elapsed)

	dataSize := float64(cfg.NumRequest) * sizeMB
	sec := elapsed.Seconds()
	throughput := float64(dataSize) / float64(sec)

	log.Printf("throughput: %0.2fMBps\n", throughput)

	return nil
}

func sendRequests(readers []io.Reader, serverURL string) time.Duration {
	var wg sync.WaitGroup
	wg.Add(len(readers))

	ch := make(chan bool)

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{
		InsecureSkipVerify: true,
	}

	for _, r := range readers {
		go func(r io.Reader) {
			defer wg.Done()

			<-ch

			//resp, err := http.Post(serverUrl, "application/octet-stream", r)
			resp, err := http.Post(serverURL, "", r)
			if err != nil {
				log.Println("http.Post", err)
				return
			}

			var buf bytes.Buffer
			tee := io.TeeReader(resp.Body, &buf)

			var payload map[string]interface{}
			if err := json.NewDecoder(tee).Decode(&payload); err != nil {
				log.Println("json umarshal error:", err)
				log.Println("response body:", buf.String())
				return
			}

			pbMsg, ok := payload["response"].(string)
			if !ok {
				log.Println("'response' field is missing")
				log.Println("response body:", buf.String())
				return
			}

			dec, err := base64.StdEncoding.DecodeString(pbMsg)
			if err != nil {
				log.Println("base64 decode:", err)
				return
			}

			var pbResp pb.Response
			if err := proto.Unmarshal([]byte(dec), &pbResp); err != nil {
				//if err := proto.Unmarshal([]byte(pbMsg), &pbResp); err != nil {
				log.Println("proto.Unmarshal:", err)
				return
			}

			log.Println("status:", resp.Status)
			log.Printf("response: %+v\n", pbResp)
		}(r)
	}

	now := time.Now()

	close(ch)
	wg.Wait()

	return time.Since(now)
}

func loadImages(dir string) (map[string][]byte, error) {
	files := make(map[string][]byte)

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		files[path] = nil
		return nil
	})

	if err != nil {
		return nil, err
	}

	for path := range files {
		var err error

		files[path], err = ioutil.ReadFile(path)
		if err != nil {
			return nil, err
		}
	}

	return files, nil
}

func newProtoData(images map[string][]byte) *pb.Request {
	req := &pb.Request{
		Machineid: "m1",
		Boardid:   "b1",
		Requestid: fmt.Sprintf("%v", time.Now().UnixNano()),
		Function:  "f1",
		Timestamp: "t1",
		Batchimgs: make([]*pb.Request_Imgdata, 0),
	}

	for filename, content := range images {
		basename := filepath.Base(filename)
		name := strings.TrimSuffix(basename, filepath.Ext(basename))
		sp := strings.Split(name, "_")

		internalpartno := "ip1"
		if len(sp) > 1 {
			internalpartno = sp[len(sp)-1]
		}

		imgData := &pb.Request_Imgdata{
			Imgid:          basename,
			Image:          content,
			Devicetype:     "dt1",
			Comppartno:     "c1",
			Internalpartno: internalpartno,
			Imgwidth:       100,
			Imghight:       100,
			Threshold:      []float64{1.0, 2.0},
		}

		req.Batchimgs = append(req.Batchimgs, imgData)
	}

	return req
}
