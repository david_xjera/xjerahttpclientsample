# Install

You will require *go1.13* to build.

**Windows**

go build -o client.exe main.go

**Linux**

go build -o client main.go

# Run

```
./client.exe [options]

Usage
  -i string
        images directory (all images will be sent in every request) (default "images")
  -n int
        number of requests to send (default 10)
  -url string
        server url (default "https://223.25.64.62:885/uploadTestData")
```

**Example**

```
./client.exe -n 4 -i myimages
```

The client will send 4 requests. It will read all files inside the "myimages" directory. Every image will be included in every request.
